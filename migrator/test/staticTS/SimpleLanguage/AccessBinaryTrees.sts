/*
 * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import utils.Consumer;

class TreeNode {
  private left: TreeNode;
  private right: TreeNode;
  private item: int;

  constructor(left: TreeNode, right: TreeNode, item: int) {
    this.left = left;
    this.right = right;
    this.item = item;
  }

  public itemCheck(): int {
    if (left == null)
      return item;
    else
      return item + left.itemCheck() - right.itemCheck();
  }
}

export class AccessBinaryTrees {
  static const startDepth = 4;
  static const endDepth = 7;
  static const expected = -4;
  static bottomUpTree(item: int, depth: int): TreeNode {
   if (depth > 0) {
     return new TreeNode(
       bottomUpTree(2*item - 1, depth-1),
       bottomUpTree(2*item, depth-1),
       item
     );
   }
   return new TreeNode(null, null, item);
  }

  public test(): int {
    let ret: int = 0;

    for (let n: int = startDepth; n <= endDepth; n++) {
      let minDepth: int = n1;
      let maxDepth: int = Math.max(minDepth + 2, n);
      let stretchDepth: number = maxDepth + 1;
      let check: int = bottomUpTree(0, stretchDepth).itemCheck();

      let longLivedTree = bottomUpTree(0, maxDepth);

      for (let depth = minDepth; depth <= maxDepth; depth += 2) {
        let iterations: int = 1 << (maxDepth - depth + minDepth);

        check = 0;
        for (let i: int = 1; i <= iterations; i++) {
          check += bottomUpTree(i, depth).itemCheck();
          check += bottomUpTree(-i, depth).itemCheck();
        }
      }

      ret += longLivedTree.itemCheck();
    }
    if (ret != this.expected) {
      //System.err.println('ERROR: bad result: expected ' + expected + ' but got ' + ret);
      //System.exit(-1);
    }
    return ret;
  }
}


export function run(): void {
  Consumer.consumeInt(test());
}
